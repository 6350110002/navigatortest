

import 'package:flutter/material.dart';
import 'package:navigatortest/model/profile.dart';
import 'package:navigatortest/screen/register.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}


class _LoginScreenState extends State<LoginScreen> {
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8,200, 8, 0),
          child: Column(
            children: [
              Text('Login',style: TextStyle(
                  fontSize:40,
                  fontWeight: FontWeight.bold,
                  color: Colors.black)),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: TextField(
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black
                  ),
                  controller: email,
                  decoration: InputDecoration(
                      hintText: 'Please Enter Your Email',
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.black,
                      ),
                      hintStyle: TextStyle(
                          fontSize: 20,
                          color: Colors.black
                      )
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: TextField(
                  obscureText: true,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black
                  ),
                  controller: password,
                  decoration: InputDecoration(
                      hintText: 'Please Enter Your Password',
                      prefixIcon: Icon(
                        Icons.lock,
                        color: Colors.black,
                      ),
                      hintStyle: TextStyle(
                          fontSize: 20,
                          color: Colors.black
                      )
                  ),
                ),
              ),
              ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.black),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )
                    )
                ),
                child: Text("Login",style: TextStyle(fontSize: 20),),
                onPressed: () async {
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => Profile_login(
                              email: email.text,
                              password: password.text)));
                },
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Don't have an account? ",
                  style: TextStyle(color: Colors.black,fontSize: 15),),
              ),
              GestureDetector(
                onTap: (){Navigator.push(context, MaterialPageRoute(
                    builder: (context){
                      return RegisterScreen();
                    })
                );
                },
                child: Text('Register',style: TextStyle(fontSize: 18,
                    fontWeight: FontWeight.bold,color: Colors.blueGrey),),
              )
            ],
          ),
        ),
      ),
    );
  }
}
