


import 'package:flutter/material.dart';
import 'package:navigatortest/screen/login.dart';
import 'package:navigatortest/screen/register.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 120, 10, 0),
        child: Column(
          children: [
            Text('WELCOME',style: TextStyle(
                fontSize:40,
                fontWeight: FontWeight.bold,
                color: Colors.black)),
            Image.asset('assets/logo.png'),
            SizedBox(
              width: 300, height: 40,
              child:ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.black),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )
                    )
                ),
                child: Text("Sign up",style: TextStyle(fontSize: 20),),
                onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return RegisterScreen();
                    })
                    );
                },
              ),

            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: 300, height: 40,
                child:ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.black),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          )
                      )
                  ),
                  child: Text("Login",style: TextStyle(fontSize: 20),),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return LoginScreen();
                    })
                    );
                  },

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
